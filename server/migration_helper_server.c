#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>		/* open */
#include<sys/mman.h>	/* mmap */
#include<strings.h>		/* bzero */
#include<string.h>

#include<time.h>

#include<linux/kvm.h>
#include<signal.h>
#include<sys/time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "minilzo.h"

#include "linux_defs.h"

//#define DEBUG

#ifdef DEBUG
#define dbg(fmt, arg...) printf(fmt,## arg)
#else
#define dbg(fmt, arg...) while(0) printf("%s : " fmt, __func__, ## arg)
#endif

#define MAX_PAGES_IN_ONE_TRIP 128
#define BLOCK_SIZE MAX_PAGES_IN_ONE_TRIP 

#define FILE_PATH "/dev/shm/ram0"
static int NUM_PAGES = 1000 ; // 10K pages is 39 MB , 25600 is 100MB on 32 bit systems
#define PAGESIZE	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))
#define NUM_INTS	(NUM_INTS_PER_PAGE*NUM_PAGES)
#define FILE_SIZE	(NUM_INTS*sizeof(int))

#define PORT 3367


int fd ; 			// the file descriptor
int *map;			// the mapping which will contain NUM_INTS integers

int count = 0;
unsigned long elements;
unsigned long hist_data_size;
int fd;
char name[]="modified kvm on the rocks";

struct dirty_log_info fetch_active_pages_log; 

struct histogram_data *d_old;
struct histogram_data *d_new;

char *qemu_server_flag = NULL;

typedef void (*sighandler_t)(int);

struct histogram_data {
	unsigned long long  max_delta_for_block;
	unsigned long long max_block_value;
	unsigned long page[BLOCK_SIZE];
}__attribute__((packed));


struct dirty_log_info {
	int index;
	unsigned long size;
	unsigned long *dirty_bitmap;
};

#define IN_LEN (PAGESIZE*128)
#define OUT_LEN (IN_LEN + IN_LEN / 16 + 64 + 3 )
#define HEAP_ALLOC(var,size) \
	    lzo_align_t __LZO_MMODEL var [ ((size) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) ]
static HEAP_ALLOC(wrkmem, LZO1X_1_MEM_COMPRESS);

#define QEMU_MIGRATION_FLAG_PATH "/dev/shm/qemu_migration_flag"
int *qemu_migration_flag;

/*

int compare2(const void *a, const void *b)
{
	struct histogram_data *d1 = (struct histogram_data *)a;
	struct histogram_data *d2 = (struct histogram_data *)b;
	return d2->max_delta_for_block - d1->max_delta_for_block;
}


void first_update_page_activity()
{
	struct timeval my_value = {0,100};
	struct timeval my_interval = {1,000};
	struct itimerval my_timer = {my_interval, my_value};

	printf("size is : %d\n",sizeof(struct histogram_data));
	int index ; 
	fd = open("/dev/kvm",O_RDWR);
	if(fd==-1)
	{	
		//perror("open");
		//exit(1);
	}
	index = ioctl(fd , KVM_GET_VM_INDEX , &name);
	printf("index = %d\n",index);

	fetch_active_pages_log.index=index;
	fetch_active_pages_log.size=0;
	int r = ioctl(fd , KVM_GET_DIRTY_LOG_SIZE_BY_VM_INDEX , &fetch_active_pages_log);
	printf("size = %lu\n",fetch_active_pages_log.size);

	fetch_active_pages_log.dirty_bitmap = malloc(fetch_active_pages_log.size*sizeof(unsigned long));
	r = ioctl(fd, KVM_GET_DIRTY_LOG_BY_VM_INDEX , &fetch_active_pages_log);
	unsigned long int active = 0;
	int i;
	int j;
	unsigned long page_freq_value;
	elements = fetch_active_pages_log.size / BLOCK_SIZE;
	hist_data_size = elements * sizeof(struct histogram_data);

	for(i=0;i<fetch_active_pages_log.size;i++)
		if(fetch_active_pages_log.dirty_bitmap[i])
			active++;
	printf("active = %lu\n",active);


	d_old = (struct histogram_data *)malloc(hist_data_size);
	d_new = (struct histogram_data *)malloc(hist_data_size);

	memset(d_old, 0, hist_data_size);


	for(i = 0;i < elements; i ++ )
	{
		page_freq_value = 0;
		for(j=0; j < BLOCK_SIZE; j++)
			page_freq_value += fetch_active_pages_log.dirty_bitmap[i * BLOCK_SIZE +j]; 
		d_old[i].max_block_value = page_freq_value; 
	}

	return;
}

void *update_pages_activity(void *arg)
{
	while(1)
	{
		//printf("called %s\n",__func__);
		//memset(d_new, 0, hist_data_size);
		int i, j;
		unsigned page_freq_value;
		if(qemu_server_flag[0] == 50)
		{
			printf("value of qemu_server_flag is : %d\n",qemu_server_flag[0]);
			return;
		}
#if 1

		int r = ioctl(fd, KVM_GET_DIRTY_LOG_BY_VM_INDEX , &fetch_active_pages_log);


		for(i = 0;i < elements; i ++ )
		{
			page_freq_value = 0;
			for(j=0; j < BLOCK_SIZE; j++)
				page_freq_value += fetch_active_pages_log.dirty_bitmap[i * BLOCK_SIZE +j]; 
			d_new[i].max_block_value = page_freq_value; 
		}

		for(i = 0; i < elements; i++)
		{
			d_new[i].max_delta_for_block = (d_new[i].max_block_value > d_old[i].max_block_value)?d_new[i].max_block_value - d_old[i].max_block_value:0;
			if(d_new[i].max_delta_for_block )
				dbg("i: %d\t old vlaue: %Lu\t new value: %Lu\t delta: %Lu\n",i, d_old[i].max_block_value, d_new[i].max_block_value, d_new[i].max_delta_for_block);
		}

		memcpy(d_old, d_new, hist_data_size);
		//if(count!=0)
		//printf("\n\n NEW VALUE BEING FETCH %d!! \n\n",count);
		count++;
		sleep(1);
#endif
	}
	return NULL;
}

*/

void setup_server();

void setup_qemu_flag()
{
	int qfd = open(QEMU_MIGRATION_FLAG_PATH,O_CREAT | O_RDWR);
	if(qfd<0){
		printf("Could not open fd for migration_flag\n");
		exit(-1);
	}
	ftruncate(qfd,sizeof(int));
	qemu_migration_flag= mmap(0,sizeof(int),PROT_READ | PROT_WRITE , MAP_SHARED , qfd , 0);
	qemu_migration_flag[0] = 0; 	// 0 indicates migration is going on
}


int main()
{
	if (lzo_init() != LZO_E_OK)
		{
			printf("internal error - lzo_init() failed !!!\n");
			printf("(this usually indicates a compiler bug - try recompiling\nwithout optimizations, and enable '-DLZO_DEBUG' for diagnostics)\n");
		}

	setup_qemu_flag();

	//pthread_t active_func_thread;
	int result;
	int i;
	fd = open(FILE_PATH , O_RDONLY);
	if(fd<0)
	{
		printf("Could not open %s. Terminating program.\n",FILE_PATH);
		exit(-1);
	}
	struct stat64 st;
	result = stat64(FILE_PATH,&st);
	unsigned long fs = st.st_size;
	NUM_PAGES = fs/PAGESIZE;
	printf("File size in MB is %lu and NUM_PAGES = %d \n",fs/1024/1024,NUM_PAGES);
	map = mmap(0,FILE_SIZE , PROT_READ , MAP_SHARED , fd , 0 );
	if(map== MAP_FAILED)
	{
		printf("Could not mmap the file.Terminating.\n");
		close(fd);
		exit(-1);
	}
	printf("mmap successful.\n");
	setup_server();
	// starting the thread
	printf("Server shutdown. Terminating program\n");
	munmap(map,(size_t)fs);
	close(fd);
	return 0;

}


void connection_has_broken()
{
	printf("%s called.\n",__func__);
	qemu_migration_flag[0] = -1;
	exit(-1);
}


void setup_server()
{
	int parentfd; /* parent socket */
	int childfd; /* child socket */
	int portno = PORT; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int n;
	unsigned long incoming_pgoff  ;
	int size_ul = sizeof(unsigned long);
	char buf[size_ul+1];					// +1 for the null byte
	int page_size = PAGESIZE;
	void *src = map;
	int pages_transferred = 0;


	// lzo stuff

	unsigned char __LZO_MMODEL in   [ IN_LEN ];
	unsigned char __LZO_MMODEL out  [ OUT_LEN ];

	unsigned long total_in = 0;
	unsigned long total_out = 0;

	lzo_uint in_len;
	lzo_uint out_len;

	time_t start , stop;
	int flag_for_time=1;


	/*
	// mmap the qemu_server_flag;
	int qemu_server_fd = open("/dev/shm/qemu_server_flag",O_RDONLY, 0600);
	if(fd < 0)
	{
		perror("open");
		exit(1);
	}
	qemu_server_flag = mmap(NULL, 2*sizeof(char), PROT_READ, MAP_SHARED, qemu_server_fd, 0);
	close(qemu_server_fd);
	if(qemu_server_flag == MAP_FAILED)
	{
		perror("mmap");
		exit(1);
	}
	*/

	//printf("value of qemu_server_flag is : %c\n",qemu_server_flag[0]);
	//first run of the data collection
	//first_update_page_activity();

	// starting the thread
	//pthread_t active_func_thread;
	//pthread_create(&active_func_thread, NULL, &update_pages_activity, NULL);
	//pthread_detach(active_func_thread);



	parentfd = socket(AF_INET, SOCK_STREAM, 0);
	if (parentfd < 0)
	{
		printf("error opening socket.Terminating program\n");
		return;
	}
	printf("parentfd opened successfully\n");
	optval = 1;
	setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,(const void *)&optval , sizeof(int));
	setsockopt(parentfd, SOL_SOCKET, SO_KEEPALIVE,(const void *)&optval , sizeof(int));
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);
	if (bind(parentfd, (struct sockaddr *) &serveraddr,sizeof(serveraddr)) < 0)
	{
		printf("error on binding\n");
		return;
	}
	printf("bind successful\n");
	if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
	{
		printf("error on listen");
		return;
	}
	printf("listen successful\n");
	clientlen = sizeof(clientaddr);
	while (1) {
		printf("waiting for a connection....\n");
		childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
		if (childfd < 0)
		{
			printf("error on accept\n");
			return;
		}
		printf("accept successful\n");
		hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		if (hostp == NULL)
			printf("error on gethostbyaddr\n");
		else
			printf("got hostp\n");
		hostaddrp = inet_ntoa(clientaddr.sin_addr);
		if (hostaddrp == NULL)
			printf("error on inet_ntoa\n");
		else
			printf("got hostaddrp\n");
		if(hostp)
			printf("server established connection with %s (%s)\n",hostp->h_name, hostaddrp);
		else if(hostaddrp)
			printf("server establisted connection with %s\n",hostaddrp);

		// will continue to send data until incoming_pgoff < 0
		printf("page_size = %d size_ul=%d\n\n",page_size,size_ul);
		int page_being_processed = 0;

		// send the NUM_PAGES to client
		unsigned long tempN = NUM_PAGES ;
		n = send(childfd , &tempN ,sizeof(unsigned long) , 0 );

		if(n<0 || n<sizeof(unsigned long)){
			connection_has_broken();
		}

		while(1)
		{
			bzero(buf, size_ul);
			printf("Waiting for pgoff...\n\n");
			n = recv(childfd, buf, size_ul , MSG_WAITALL);
			if (n < 0 || n < size_ul)
			{
				printf("error reading from socket\n");
				connection_has_broken();
				break;
			}

			buf[size_ul]=0;								// assign the null byte
			//printf("page_being_processed : %d\n",page_being_processed );
			int i=0;
			incoming_pgoff = 0;
			char *tp = (char*)&incoming_pgoff;
			while(i<size_ul)
			{
				*tp=buf[i];
				tp++;
				i++;
			}
			if(incoming_pgoff < 0 )
			{
				printf("Since pgoff < 0 , terminating connection and execution of program.\n");
				break;
			}
			if(incoming_pgoff >= NUM_PAGES)
			{
				printf("Since pgoff is not in range ( pgoff = %lu NUM_PAGES = %d ) , terminating connection and execution of program.\n",incoming_pgoff,NUM_PAGES);
				break;
			}
			printf("pgoff recv = %lu\n",incoming_pgoff);
			int max_to_be_sent = (incoming_pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - incoming_pgoff;
			int data_to_be_sent = page_size*max_to_be_sent ;
			int offset = 0;


			// before we send , let's compress it
			memcpy(in,src + incoming_pgoff*page_size,data_to_be_sent);
			in_len = data_to_be_sent;
			total_in+=in_len;
			int r = lzo1x_1_compress(in,in_len,out,&out_len,wrkmem);
			if (r == LZO_E_OK)
				printf("compressed %lu bytes into %lu bytes\n",(unsigned long) in_len, (unsigned long) out_len);
			total_out+=out_len;

			// send length of compressed data
			r = send(childfd,&out_len,size_ul,0);

			if(r<0 || r < size_ul)
				connection_has_broken();

			// send the compressed data

			while(out_len)
			{
				n = send (childfd , out , out_len , 0 ) ;
				if(n<0)
					connection_has_broken();
				out_len -= n ;
				offset += n;
				printf("data sent %d\n",n);
			}
			/*
			if (offset < page_size*max_to_be_sent)
			{
				printf("error writing to socket\n");
				break;
			}
			*/
			pages_transferred+=max_to_be_sent;
			page_being_processed+=max_to_be_sent;
			if(flag_for_time==1 && pages_transferred>1248){
				flag_for_time=0;
				time(&start);
			}
			if(pages_transferred%5000)
				printf("pages_transferred = %d\n",pages_transferred);
			if(page_being_processed == NUM_PAGES)
				break;
		}
		close(childfd);
		printf("Pages transferred = %d\n",pages_transferred);
		break;	// remove comment if pages are to be transferred only once
	}
	close(parentfd);
	time(&stop);
	printf("All pages sent in approx. %.0f seconds \n",difftime(stop,start));
	return;
}




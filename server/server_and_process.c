#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>		/* open */
#include<sys/mman.h>	/* mmap */
#include<strings.h>		/* bzero */
#include<string.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>


#define FILE_PATH "process_memory_holder"
#define NUM_PAGES	51200 // 10K pages is 39 MB , 25600 is 100MB on 32 bit systems
#define PAGESIZE	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))
#define NUM_INTS	(NUM_INTS_PER_PAGE*NUM_PAGES)
#define FILE_SIZE	(NUM_INTS*sizeof(int))
int fd ; 			// the file descriptor
int *map;			// the mapping which will contain NUM_INTS integers

#define MAX_PAGES_IN_ONE_TRIP 128

#define PORT 3367

void setup_server();

int main()
{
	int result;
	int i;
	fd = open(FILE_PATH , O_RDWR | O_CREAT | O_TRUNC);
	if(fd<0)
	{
		printf("Could not open %s. Terminating program.\n",FILE_PATH);
		exit(-1);
	}
	printf("File size in MB is %lu\n",FILE_SIZE/1024/1024);
	result = lseek(fd , FILE_SIZE-1 ,SEEK_SET );
	if(result<0)
	{
		close(fd);
		printf("Could not 'stretch' the file. Terminating program.\n");
		exit(-1);
	}
	result=write(fd,"",1);	// write an empty file
	if(result!=1)
	{
		close(fd);
		printf("error writing to file.Terminating program\n");
		exit(-1);
	}
	printf("File created. \n");
	//scanf("%*d");
	map = mmap(0,FILE_SIZE , PROT_READ | PROT_WRITE , MAP_PRIVATE , fd , 0 );
	if(map== MAP_FAILED)
	{
		printf("Could not mmap the file.Terminating.\n");
		close(fd);
		exit(-1);
	}
	printf("mmap successful.\n");
	printf("Initialising the array..this may take time\n");
	for(i=0;i<NUM_INTS;i++)
	{
		map[i]=i+1;
		if(i<10)
			printf("map[%d]=%d\n",i,map[i]);
	}
	printf("map initialiased. map[0]=%d map[%lu]=%d Starting server...\n",map[0],NUM_INTS-1,map[NUM_INTS-1]);
	setup_server();
	printf("Server shutdown. Terminating program\n");
	munmap(map,FILE_SIZE);
	close(fd);
	if(remove(FILE_PATH) == -1)
	{
		printf("Could not remove %s\n",FILE_PATH);
		return -1;
	}
	return 0;

}
void setup_server()
{
	int parentfd; /* parent socket */
	int childfd; /* child socket */
	int portno = PORT; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int n;
	unsigned long incoming_pgoff  ;
	int size_ul = sizeof(unsigned long);
	char buf[size_ul+1];					// +1 for the null byte
	int page_size = PAGESIZE;
	void *src = map;
	int pages_transferred = 0;

    parentfd = socket(AF_INET, SOCK_STREAM, 0);
    if (parentfd < 0)
    {
	    printf("error opening socket.Terminating program\n");
	    return;
    }
    printf("parentfd opened successfully\n");
    optval = 1;
    setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,(const void *)&optval , sizeof(int));
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons((unsigned short)portno);
    if (bind(parentfd, (struct sockaddr *) &serveraddr,sizeof(serveraddr)) < 0)
    {
        printf("error on binding\n");
        return;
    }
    printf("bind successful\n");
    if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
    {
        printf("error on listen");
        return;
    }
    printf("listen successful\n");
    clientlen = sizeof(clientaddr);
    while (1) {
    	printf("waiting for a connection....\n");
    	childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
    	if (childfd < 0)
    	{
    	   printf("error on accept\n");
    	   return;
    	}
    	printf("accept successful\n");
    	hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		if (hostp == NULL)
			printf("error on gethostbyaddr\n");
		else
			printf("got hostp\n");
		hostaddrp = inet_ntoa(clientaddr.sin_addr);
		if (hostaddrp == NULL)
			printf("error on inet_ntoa\n");
		else
			printf("got hostaddrp\n");
		if(hostp)
			printf("server established connection with %s (%s)\n",hostp->h_name, hostaddrp);
		else if(hostaddrp)
			printf("server establisted connection with %s\n",hostaddrp);

		// will continue to send data until incoming_pgoff < 0
		printf("page_size = %d size_ul=%d\n\n",page_size,size_ul);
		int page_being_processed = 0;

		// send the NUM_PAGES to client
		unsigned long tempN = NUM_PAGES ;
		n = send(childfd , &tempN ,sizeof(unsigned long) , 0 );

		while(1)
		{
			bzero(buf, size_ul);
			printf("Waiting for pgoff...\n\n");
			n = recv(childfd, buf, size_ul , MSG_WAITALL);
			if (n < 0)
			{
			  printf("error reading from socket\n");
			  break;
			}

			buf[size_ul]=0;								// assign the null byte
			//printf("page_being_processed : %d\n",page_being_processed );
			int i=0;
			incoming_pgoff = 0;
			char *tp = (char*)&incoming_pgoff;
			while(i<size_ul)
			{
				*tp=buf[i];
				tp++;
				i++;
			}
			if(incoming_pgoff < 0 )
			{
				printf("Since pgoff < 0 , terminating connection and execution of program.\n");
				break;
			}
			if(incoming_pgoff >= NUM_PAGES)
			{
				printf("Since pgoff is not in range ( pgoff = %lu NUM_PAGES = %d ) , terminating connection and execution of program.\n",incoming_pgoff,NUM_PAGES);
				break;
			}
			printf("pgoff recv = %lu\n",incoming_pgoff);
			int max_to_be_sent = (incoming_pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - incoming_pgoff;
			int data_to_be_sent = page_size*max_to_be_sent ;
			int offset = 0;
			while(data_to_be_sent)
			{
				n = send (childfd , src + incoming_pgoff*page_size + offset , data_to_be_sent , 0 ) ;
				data_to_be_sent -= n ;
				offset += n;
				printf("data sent %d\n",n);
			}
			if (offset < page_size*max_to_be_sent)
			{
			      printf("error writing to socket\n");
			      break;
			}
			pages_transferred+=max_to_be_sent;
			page_being_processed+=max_to_be_sent;
			if(pages_transferred%5000)
				printf("pages_transferred = %d\n",pages_transferred);
			if(page_being_processed == NUM_PAGES)
				break;
		}
		close(childfd);
		printf("Pages transferred = %d\n",pages_transferred);
		//break;	// remove comment if pages are to be transferred only once
    }
    close(parentfd);
    return;
}




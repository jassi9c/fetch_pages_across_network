#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>		/* open */
#include<sys/mman.h>	/* mmap */
#include<strings.h>		/* bzero */
#include<string.h>

#include<linux/kvm.h>
#include<signal.h>
#include<sys/time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#define FILE_PATH "/dev/shm/ram0"
static int NUM_PAGES = 1000 ; // 10K pages is 39 MB , 25600 is 100MB on 32 bit systems
#define PAGESIZE	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))
#define NUM_INTS	(NUM_INTS_PER_PAGE*NUM_PAGES)
#define FILE_SIZE	(NUM_INTS*sizeof(int))

#define PORT 3368

int fd ; 			// the file descriptor
int *map;			// the mapping which will contain NUM_INTS integers

#define QEMU_MIGRATION_FLAG_PATH "/dev/shm/qemu_migration_flag"

int *qemu_flag;

unsigned long total_in  = 0;

// first start migration helper and it sets up the above flag equal to 0
// 0 means migration is going on
// 1 means successfully completed migration
// -1 means migration was unsuccessful and in this case , vm on src should be started again
// any non-zero value means we should break the connection with the dst

// after we get each page , we check the flag and also send it's value back as an ack.
// this way , destination knows when to stop

// the value of qemu_migration_flag is set and updated in migration server

void open_qemu_flag()
{
	int qfd = open(QEMU_MIGRATION_FLAG_PATH,O_RDONLY);
	if(qfd<0){
		printf("Could not open qemu migration flag.\n");
		exit(-1);
	}
	qemu_flag=mmap(0,sizeof(int),PROT_READ,MAP_SHARED,qfd,0);
	close(qfd);
}

void setup_server();

int main()
{
	open_qemu_flag();
	int result;
	int i;
	fd = open(FILE_PATH , O_RDWR);
	if(fd<0)
	{
		printf("Could not open %s. Terminating program.\n",FILE_PATH);
		exit(-1);
	}
	struct stat64 st;
	result = stat64(FILE_PATH,&st);
	unsigned long fs = st.st_size;
	NUM_PAGES = fs/PAGESIZE;
	printf("File size in MB is %lu and NUM_PAGES = %d \n",fs/1024/1024,NUM_PAGES);
	map = mmap(0,FILE_SIZE , PROT_READ | PROT_WRITE, MAP_SHARED , fd , 0 );
	if(map== MAP_FAILED)
	{
		printf("Could not mmap the file.Terminating.\n");
		close(fd);
		exit(-1);
	}
	printf("mmap successful.\n");
	setup_server();
	return 0;
}


void setup_server()
{
	int parentfd; /* parent socket */
	int childfd; /* child socket */
	int portno = PORT; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int n;
	unsigned long incoming_pgoff  ;
	int size_ul = sizeof(unsigned long);
	char buf[size_ul+1];					// +1 for the null byte
	int page_size = PAGESIZE;
	void *src = map;
	int pages_transferred = 0;

	parentfd = socket(AF_INET, SOCK_STREAM, 0);
	if (parentfd < 0)
	{
		printf("error opening socket.Terminating program\n");
		return;
	}
	printf("parentfd opened successfully\n");
	optval = 1;
	setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,(const void *)&optval , sizeof(int));
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);
	if (bind(parentfd, (struct sockaddr *) &serveraddr,sizeof(serveraddr)) < 0)
	{
		printf("error on binding\n");
		return;
	}
	printf("bind successful\n");
	if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
	{
		printf("error on listen");
		return;
	}
	printf("listen successful\n");
	clientlen = sizeof(clientaddr);
	while (1) {
			printf("waiting for a connection....\n");
			childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
			if (childfd < 0)
			{
				printf("error on accept\n");
				return;
			}
			printf("accept successful\n");
			hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
			if (hostp == NULL)
				printf("error on gethostbyaddr\n");
			else
				printf("got hostp\n");
			hostaddrp = inet_ntoa(clientaddr.sin_addr);
			if (hostaddrp == NULL)
				printf("error on inet_ntoa\n");
			else
				printf("got hostaddrp\n");
			if(hostp)
				printf("server established connection with %s (%s)\n",hostp->h_name, hostaddrp);
			else if(hostaddrp)
				printf("server establisted connection with %s\n",hostaddrp);
			printf("page_size = %d size_ul=%d\n\n",page_size,size_ul);

			int page_being_processed = 0;
			while(1){
				bzero(buf, size_ul);
				printf("Waiting for pgoff...\n\n");
				n = recv(childfd, buf, size_ul , MSG_WAITALL);
				if (n < 0)
				{
					printf("error reading from socket\n");
					break;
				}

				buf[size_ul]=0;								// assign the null byte
				//printf("page_being_processed : %d\n",page_being_processed );
				int i=0;
				incoming_pgoff = 0;
				char *tp = (char*)&incoming_pgoff;
				while(i<size_ul)
				{
					*tp=buf[i];
					tp++;
					i++;
				}
				if(incoming_pgoff < 0 )
				{
					printf("Since pgoff < 0 , terminating connection and execution of program.\n");
					break;
				}
				if(incoming_pgoff >= NUM_PAGES)
				{
					printf("Since pgoff is not in range ( pgoff = %lu NUM_PAGES = %d ) , terminating connection and execution of program.\n",incoming_pgoff,NUM_PAGES);
					break;
				}
				printf("pgoff recv = %lu  ",incoming_pgoff);
				n = recv(childfd , map+incoming_pgoff*NUM_INTS_PER_PAGE, PAGESIZE , MSG_WAITALL);
				if(n<0){
					printf("Error on recv.\n");
					printf(strerror(errno));
					exit(-1);
				}
				puts("page recv.\n");
				total_in++;
				//send(childfd , &qemu_flag[0] , sizeof(int),0);
				//if(qemu_flag[0]){	// migration was either successful or has failed , notify to stop sending more page
				//	break;
				//}
			}
			close(childfd);
			break;
	}
	close(parentfd);
	printf("Total pages recv. = %lu\n",total_in);
	return;
}

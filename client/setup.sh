make clean-all
rmmod driver
make
insmod driver.ko
rm -rf fault_handler
mknod fault_handler c `grep fault_handler /proc/devices | awk '{print $1}'` 0
make clean
cc -o check check.c -g -lpthread
cc -o helper helper.c -g -lpthread
cc -c minilzo.c
cc -o newhelper minilzo.o newhelper.c -g -lpthread

/*
 * Comments : 	Clean and working version of driver based on zfd method.
 * 				Do NOT change this file.Instead make a new copy and experiment with it.
 * 				There are no print statements except to indicate functionality. No debug code is present.
 * 				Code based on vmem driver of AIST people , simplified by Sanidhya.
 * 				Comments have been removed except where necessary , to improve readability.
 * 				Kernel coding style is followed.
 * 				Name of variable indicates what it does.
 * 				Global variables are declared static.
 * 				Error handling has been done where needed.
 * 				A return 0 means successful execution.
 *
 * 				Name of device is set to 'fault_handler' in fault_hander.h
 * 				Custom functions of device start with the prefix 'fh_'
 *
 */


#include<linux/init.h>
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/sched.h>
#include<linux/vmalloc.h>
#include<linux/fs.h>
#include<asm/uaccess.h>
#include<linux/ioctl.h>
#include<linux/wait.h>
#include<linux/mm.h>
#include<linux/slab.h>
#include<linux/kfifo.h>
#include<linux/spinlock.h>

#include "fault_handler.h"

static int fh_open(struct inode * , struct file *);
static int fh_release(struct inode *, struct file *);
static int fh_mmap(struct file *, struct vm_area_struct *);
static long fh_ioctl(struct file *, unsigned int , unsigned long );
static int fh_vm_pagefault(struct vm_area_struct *, struct vm_fault *);
int fh_reserve_memory(unsigned long , unsigned long );

static int major_device_number ;
static int device_open ;

DECLARE_WAIT_QUEUE_HEAD(wait_for_faults_queue_head);
DECLARE_WAIT_QUEUE_HEAD(faults_processed_queue_head);
DECLARE_WAIT_QUEUE_HEAD(wait_for_user_process_to_start);
static struct kfifo faults_queue;
DEFINE_SPINLOCK(faults_queue_lock);

static unsigned long *bitmap_faults_processed;
static unsigned long *bitmap_faults_pending;

static unsigned long **reserved_pages = NULL;
static unsigned long total_pages_reserved_by_user = 0 ;
static unsigned long total_pages_fetched = 0;

static unsigned long size_ul = sizeof(unsigned long);

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = fh_open,
	.release = fh_release,
	.mmap = fh_mmap,
	.unlocked_ioctl = fh_ioctl,
};


static struct vm_operations_struct vm_ops = {
	.fault = fh_vm_pagefault,
};


int init_module(void)
{

	major_device_number = register_chrdev(0, DEVICE_NAME, &fops);

	if (major_device_number < 0) {
	  printk(KERN_ALERT "Registering char device failed with %d\n", major_device_number);
	  return major_device_number;
	}

	printk(KERN_INFO "I was assigned major number %d. To talk to\n", major_device_number);
	printk(KERN_INFO "the driver, create a dev file with\n");
	printk(KERN_INFO "'mknod %s c %d 0'.\n", DEVICE_NAME, major_device_number);
	printk(KERN_INFO "Remove the device file and module when done.\n");
	return 0;
}

void cleanup_module(void)
{
	if(reserved_pages)
		vfree(reserved_pages);
	if(bitmap_faults_pending)
		kfree(bitmap_faults_pending);
	if(bitmap_faults_processed)
		kfree(bitmap_faults_processed);
	unregister_chrdev(major_device_number, DEVICE_NAME);
	printk("Module removed successfully\n");
}


static int fh_open(struct inode *inode, struct file *file)
{
	printk("%s called\n",__func__);
	try_module_get(THIS_MODULE);
	return 0;
}

static int fh_release(struct inode *inode, struct file *file)
{
	printk("%s called\n",__func__);
	total_pages_reserved_by_user =0;
	module_put(THIS_MODULE);
	return 0;
}

static int fh_mmap(struct file *filp, struct vm_area_struct *vma)
{
	vma->vm_ops = &vm_ops;
	return 0;
}

static long fh_ioctl(struct file *filp, unsigned int ioctl, unsigned long arg)
{
	int ret;
	int i;
	int max_processed ;
	struct fh_reserve_memory_request set_mem_info;
	unsigned long pgoff_to_be_processed;
	unsigned long pgoff_processed;
	switch(ioctl)
	{
		case FAULT_HANLDER_GET_NPAGES :
			printk("Waiting to get number of pages\n");
			wait_event_interruptible(wait_for_user_process_to_start , total_pages_reserved_by_user !=0 );
			copy_to_user((void*)arg , &total_pages_reserved_by_user , size_ul);
			break;
		case FAULT_HANDLER_RESERVE_MEMORY:
			ret = copy_from_user(&set_mem_info, (void*)arg, sizeof(struct fh_reserve_memory_request));
			return fh_reserve_memory(set_mem_info.addr, set_mem_info.npages);
			break;

		case FAULT_HANDLER_GET_FAULT:
			wait_event_interruptible(wait_for_faults_queue_head,!kfifo_is_empty(&faults_queue));
			spin_lock(&faults_queue_lock);
			kfifo_out(&faults_queue,(void *)&pgoff_to_be_processed, size_ul);
			spin_unlock(&faults_queue_lock);
			copy_to_user((void *)arg, &pgoff_to_be_processed, size_ul);
			break;

		case FAULT_HANDLER_FAULT_PROCESSED:
			copy_from_user(&pgoff_processed, (void *)arg, size_ul);
			max_processed = (pgoff_processed < total_pages_reserved_by_user - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : total_pages_reserved_by_user - pgoff_processed ;
			spin_lock(&faults_queue_lock);
			total_pages_fetched+=max_processed;
			set_bit(pgoff_processed/MAX_PAGES_IN_ONE_TRIP, bitmap_faults_processed);
			wake_up_interruptible_all(&faults_processed_queue_head);
			clear_bit((pgoff_processed/MAX_PAGES_IN_ONE_TRIP), bitmap_faults_pending);
			spin_unlock(&faults_queue_lock);
			wake_up_interruptible(&faults_processed_queue_head);
			if(total_pages_fetched>=total_pages_reserved_by_user)	// tell helper all pages have been fetched
			{
				pgoff_processed=-1;
				spin_lock(&faults_queue_lock);
				kfifo_in(&faults_queue, (void *)&pgoff_processed, size_ul);
				spin_unlock(&faults_queue_lock);
				wake_up_interruptible(&wait_for_faults_queue_head);
			}
			break;
		default:
			return -ENOTTY;
	}
	return 0;
}



int fh_reserve_memory(unsigned long u_addr, unsigned long u_total_pages_reserved_by_user)
{

	unsigned long ret;

	total_pages_reserved_by_user 	= u_total_pages_reserved_by_user;
	reserved_pages 					= vmalloc(total_pages_reserved_by_user*sizeof(struct page *));
	bitmap_faults_processed 		= kzalloc(total_pages_reserved_by_user/MAX_PAGES_IN_ONE_TRIP,GFP_KERNEL);
	bitmap_faults_pending   		= kzalloc(total_pages_reserved_by_user/MAX_PAGES_IN_ONE_TRIP,GFP_KERNEL);
	total_pages_fetched				= 0;


	down_read(&current->mm->mmap_sem);
	ret = get_user_pages(current, current->mm, u_addr, total_pages_reserved_by_user, 1, 0, (struct page **)reserved_pages, NULL);
	up_read(&current->mm->mmap_sem);

	if(ret != total_pages_reserved_by_user)
		return -EINVAL;
	ret = kfifo_alloc(&faults_queue, PAGE_SIZE, GFP_KERNEL);
	if(ret)
		return ret;

	return 0;
}


static int fh_vm_pagefault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
	unsigned long pgoff;
	unsigned long j;
	if(vmf->page) {
		//printk("page already fetched : %lu and present and total_pages_fetched = %lu\n",vmf->pgoff,  total_pages_fetched);
		vmf->page = reserved_pages[vmf->pgoff];
		goto out;
	}
	vmf->page = reserved_pages[vmf->pgoff];
	if(test_bit(vmf->pgoff/MAX_PAGES_IN_ONE_TRIP, bitmap_faults_processed)){
		//printk("page already fetched : %lu and present and total_pages_fetched = %lu\n",vmf->pgoff,total_pages_fetched);
		goto out;
	}


	spin_lock(&faults_queue_lock);
	if(test_and_set_bit(vmf->pgoff/MAX_PAGES_IN_ONE_TRIP, bitmap_faults_pending))	{
			spin_unlock(&faults_queue_lock);
			goto wait_out;
	}
	pgoff = vmf->pgoff - vmf->pgoff%MAX_PAGES_IN_ONE_TRIP;
	//printk("Inserting %lu in queue and total_pages_fetched = %lu\n",pgoff,total_pages_fetched);
	kfifo_in(&faults_queue, (void *)&pgoff, size_ul);
	spin_unlock(&faults_queue_lock);
	wake_up_interruptible(&wait_for_faults_queue_head);

wait_out:
	wait_event_interruptible(faults_processed_queue_head, test_bit(vmf->pgoff/MAX_PAGES_IN_ONE_TRIP, bitmap_faults_processed));
out:
	get_page(vmf->page);
	return 0;
}

MODULE_LICENSE("GPL");

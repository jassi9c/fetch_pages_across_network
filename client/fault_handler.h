#ifndef FAULT_HANDLER_H_
#define FAULT_HANDLER_H_

#define DEVICE_NAME "fault_handler"

#define MAX_PAGES_IN_ONE_TRIP 128

struct fh_reserve_memory_request {
	unsigned long addr;
	unsigned long npages;
};


struct dirty_log_by_index {
	int index;
	unsigned long size;
	unsigned long *dirty_bitmap;
};

#define FAULT_HANDLER_MAGIC 'f'
#define FAULT_HANDLER_RESERVE_MEMORY 			_IOWR(FAULT_HANDLER_MAGIC, 1, struct fh_reserve_memory_request)
#define FAULT_HANDLER_GET_FAULT 				_IOWR(FAULT_HANDLER_MAGIC, 2, unsigned long)
#define FAULT_HANDLER_FAULT_PROCESSED			_IOW (FAULT_HANDLER_MAGIC, 3, unsigned long)
#define FAULT_HANLDER_GET_NPAGES				_IOWR(FAULT_HANDLER_MAGIC, 4, unsigned long)

#define KVMIO 0xAE
#define KVM_GET_VM_INDEX          		 _IOWR(KVMIO,   0x31 , unsigned long) // pass the name
#define KVM_GET_DIRTY_LOG_SIZE_BY_VM_INDEX          		 _IOWR(KVMIO,   0x32 , struct dirty_log_by_index)
#define KVM_GET_NEW_DIRTY_LOG_BY_VM_INDEX	 _IOWR(KVMIO,   0x36 , struct dirty_log_by_index)

#endif /* FAULT_HANDLER_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "fault_handler.h"

/*
 * borrowed definitions from the linux kernel
 */
#define BIT(nr)				(1UL << (nr))
#define BIT_MASK(nr)		(1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)
#define DIV_ROUND_UP(n,d)	(((n)+(d)-1) / (d))
#define BITS_PER_BYTE		8
#define BITS_TO_LONG(nr)	DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(long))
#define BITS_PER_LONG		(sizeof(unsigned long) * BITS_PER_BYTE)


#define PAGESIZE 	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))

#define PORT 3367
#define hostname "10.3.3.43"

static unsigned long NUM_PAGES ;

static int fd;
static int sockfd ;
static int *map;
static unsigned long size_ul = sizeof ( unsigned long );

unsigned long *pages_bitmap;

pthread_mutex_t bgod_mutex  = PTHREAD_MUTEX_INITIALIZER ;
pthread_cond_t  bgod_cond   = PTHREAD_COND_INITIALIZER;
static int od_ready = 0;
static int bg_can_start = 1;
static int total_fetched = 0;

/*
 * create_bitmap - to create the bitmap
 */
unsigned long *create_bitmap(unsigned long bits)
{
	unsigned long *bitmap_array;
	bitmap_array = (unsigned long *)malloc(BITS_TO_LONG(bits) * sizeof(unsigned long));
	memset(bitmap_array, 0, sizeof(unsigned long)* BITS_TO_LONG(bits));
	return bitmap_array;
}


/*
 * set_bit - to set the bit of the given offset
 * it is same as that of linux kernel
 */
static void set_bit(unsigned long bit, unsigned long *bitmap_array)
{
	unsigned long mask = BIT_MASK(bit);
	unsigned long *p = ((unsigned long *)bitmap_array) + BIT_WORD(bit);
	*p |= mask;
	return;
}

/*
 * test_bit - to check if the bit is set or not
 */
static int test_bit(unsigned long bit, unsigned long *bitmap_array)
{
	unsigned long mask = BIT_MASK(bit);
	unsigned long *p = ((unsigned long *)bitmap_array) + BIT_WORD(bit);
	return ((*p & mask) != 0);
}

/* 
 * test_and_set_bit - to test the older bit and set it before returning 
 * true or false
 */

static int test_and_set_bit(unsigned long bit, unsigned long *bitmap_array)
{
	unsigned long mask = BIT_MASK(bit);
	unsigned long *p = ((unsigned long *)bitmap_array) + BIT_WORD(bit);
	unsigned long old;
	old = *p;
	*p |= mask;
	return ((old & mask) != 0);
}


/*
 * clear_bitmap - to free the memory of the bitmap
 */

static void clear_bitmap(unsigned long *bitmap_array)
{
	free(bitmap_array);
	bitmap_array=NULL;
	return;
}

int setup_conn()
{
	int n;
	struct sockaddr_in server_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		printf("socket connection error");
		return -1;
	}
	server = gethostbyname(hostname);
	if(server == NULL)
	{
		printf("No such host exists.. EXITING");
		exit(0);
	}
	memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	memmove((char *)&server_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
	server_addr.sin_port = htons(PORT);
	if(connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		perror("connect");
		return -1;
	}

	n = recv(sockfd , &NUM_PAGES , sizeof(unsigned long) , MSG_WAITALL);
	printf("Got %lu as NUM_PAGES from server\n",NUM_PAGES);

	return 0;
}

int reserve_memory_address_space()
{
	int zfd;
	int ret ;
	struct fh_reserve_memory_request mem_req ;
	zfd = open("/dev/zero",O_RDWR | O_TRUNC);
	if(zfd < 0)
	{
		perror("open");
		return -ENOMEM;
	}
	printf("Reserving %lu bytes and npages = %lu\n",NUM_PAGES*PAGESIZE,NUM_PAGES);
	map = mmap(NULL, NUM_PAGES*PAGESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, zfd, 0);

	if(map == MAP_FAILED)
	{
		close(zfd);
		perror("mmap");
		return -ENOMEM;
	}
	close(zfd);

	mem_req.addr = (unsigned long)map;
	mem_req.npages = NUM_PAGES;
	ret = ioctl(fd, FAULT_HANDLER_RESERVE_MEMORY, &mem_req);
	if(ret)
	{
		perror("ioctl");
		close(fd);
		munmap(map, NUM_PAGES*PAGESIZE);
		return 1;
	}
	return 0;
}

void* bg_function(void *arg){

	unsigned long last_pgoff = 0;
	int ret;
	int max_processed;
	int pages_fetched  = 0;
	printf("bg thread starting....\n");
	while(1){
			if(od_ready){
				pthread_cond_signal(&bgod_cond);
			}
			pthread_mutex_lock(&bgod_mutex);

			if(last_pgoff>=NUM_PAGES){
				pthread_mutex_unlock(&bgod_mutex);
				break;
			}

			//if(pages_bitmap[last_pgoff]==1)
			if(test_bit(last_pgoff, pages_bitmap))
			{
				last_pgoff+=MAX_PAGES_IN_ONE_TRIP;
				pthread_mutex_unlock(&bgod_mutex);
				continue;
			}
			//printf("bg : pgoff = %lu pages_bitmap[%lu]=%d\n",last_pgoff,last_pgoff,pages_bitmap[last_pgoff]);
			ret = send(sockfd, &last_pgoff, size_ul, 0);
			if(ret <= 0)
			{
				printf("socket write error");
				close(fd);
				munmap(map, NUM_PAGES*PAGESIZE);
				exit(-1);
			}
			max_processed = (last_pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - last_pgoff ;
			ret = recv(sockfd, map + last_pgoff * NUM_INTS_PER_PAGE, PAGESIZE*max_processed, MSG_WAITALL);
			if(ret < 0)
			{
				printf("socket read error");
				close(fd);
				munmap(map, NUM_PAGES*PAGESIZE);
				exit(-1);
			}
			unsigned long nret;
			for(nret=last_pgoff;nret<last_pgoff+max_processed;nret++){
				//if(nret==last_pgoff)
					//printf("bg : setting pages_bitmap[%lu]=1\n",nret);
				//pages_bitmap[nret]=1;
				set_bit(nret, pages_bitmap);
			}
			pages_fetched+=max_processed;
			total_fetched+=max_processed;
			//printf("bg: pgoff = %lu done and pages_bitmap[%lu]=%d\n",last_pgoff,last_pgoff,pages_bitmap[last_pgoff]);
			//printf("bg: total_fetched = %d\n",total_fetched);
			pthread_mutex_unlock(&bgod_mutex);
			pthread_cond_signal(&bgod_cond);
			last_pgoff+=128;
			if(last_pgoff>=NUM_PAGES || total_fetched>=NUM_PAGES)
				break;
	}
	printf("bg: pages fetched  : %d\n",pages_fetched);
	return NULL;
}

int helper_fn()
{
	printf("%s executing\n",__func__);
	unsigned long pgoff;
	int ret;
	int pages_transferred = 0;
	int max_processed ;
	int total_data =0;
	int pages_fetched = 0;
	pthread_t bg_thread;
	while(1)
	{

		ret = ioctl(fd, FAULT_HANDLER_GET_FAULT, &pgoff);
		if(ret<0)
		{
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			printf("ioctl error");
			return 1;
		}
		od_ready=1;
		if(pthread_mutex_trylock(&bgod_mutex)){
			// if we don't get the lock , then sleep on the condition and wait for bg to complete
			pthread_cond_wait(&bgod_cond,&bgod_mutex);
		}
		//if(pages_bitmap[pgoff]==1)
		if(test_bit(pgoff, pages_bitmap))
		{
			pages_transferred+=MAX_PAGES_IN_ONE_TRIP;
			goto done;
		}
		//printf("od : pgoff = %lu  pages_bitmap[%lu]=%d\n",pgoff,pgoff,pages_bitmap[pgoff]);
		ret = send(sockfd, &pgoff, size_ul, 0);
		if(ret <= 0)
		{
			printf("socket write error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		max_processed = (pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - pgoff ;
		ret = recv(sockfd, map + pgoff * NUM_INTS_PER_PAGE, PAGESIZE*max_processed, MSG_WAITALL);
		total_data +=ret;
		if(ret < 0)
		{
			printf("socket read error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		pages_transferred+=max_processed;
		pages_fetched+=max_processed;
		total_fetched+=max_processed;
		unsigned long nret;
		for(nret=pgoff;nret<pgoff+max_processed;nret++){
			//if(nret==pgoff)
				//printf("od : setting pages_bitmap[%lu]=1\n",nret);
			//pages_bitmap[nret]=1;
			set_bit(nret, pages_bitmap);
		}

		done:
		//printf("od : pgoff = %lu sending to ioctl\n",pgoff);
		ioctl(fd, FAULT_HANDLER_FAULT_PROCESSED,&pgoff);
		od_ready=0;
		if(bg_can_start){
			pthread_create(&bg_thread , NULL , &bg_function , NULL);
			bg_can_start=0;
		}
		//printf("od : pgoff = %lu done pages_bitmap[%lu]=%d\n",pgoff,pgoff,pages_bitmap[pgoff]);
		//printf("od: total_fetched = %d\n",total_fetched);
		if(pages_transferred>=NUM_PAGES)
		{
			pthread_mutex_unlock(&bgod_mutex);
			break;
		}
		pthread_mutex_unlock(&bgod_mutex);
		
	}
	printf("od : pages fetched : %d\n",pages_fetched);
	pthread_join(bg_thread,NULL);
	return 0;
}

int main()
{
	int ret;
	unsigned long pgoff;

	if(setup_conn() < 0)
	{
		printf("Error in connection ");
		close(fd);
		return 1;
	}
	fd = open(DEVICE_NAME,O_RDWR);
	reserve_memory_address_space();
	//pages_bitmap=(char*)calloc(NUM_PAGES,1);
	pages_bitmap = create_bitmap(NUM_PAGES);
	printf("pages_bitmap initialized. pages_bitmap[0]=%d\n",pages_bitmap[0]);
	if(helper_fn())
	{
		munmap(map, NUM_PAGES*PAGESIZE);
		close(fd);
		return -1;
	}
	printf("done\n");
	munmap(map, NUM_PAGES*PAGESIZE);
	printf("munmap done\n");
	close(fd);
	close(sockfd);
	//free(pages_bitmap);
	clear_bitmap(pages_bitmap);
	return 0;
}

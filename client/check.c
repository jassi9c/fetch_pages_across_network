
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>

#include "fault_handler.h"

#define PAGES 51200
#define NUM_THREADS 500
int *map;
#define PAGE_SIZE  getpagesize()
#define NUM_INTS_PER_PAGE (PAGE_SIZE/sizeof(int))
#define NUM_INTS	(NUM_INTS_PER_PAGE*PAGES)
#define FILE_SIZE	(NUM_INTS*sizeof(int))

void * seq_fault_func(void *arg)
{
	printf("%s executing\n",__func__);
	unsigned long j;
	int temp ;
	for(j = 0; j < PAGES; j++)
	{
		temp = *(map+j*NUM_INTS_PER_PAGE);
		//if(j%128==0)
			//printf("map[%lu] : %lu Page : %d  \n ",j*NUM_INTS_PER_PAGE,map[j*NUM_INTS_PER_PAGE],j);

		/*
		if(j%5000==-1)
		{
			// simulate a delay of some seconds , to let the bg thread do work
			puts("delay start");
			int k,l,p;
			for(p=0;p<10;p++)
				for(k=0;k<10000;k++)
					for(l=0;l<10000;l++)
						temp=12312*312-1231+232/232+22;
			puts("delay end");
		}
		*/

	}
	printf("%s execution done\n",__func__);
	return arg;
}

void *random_fault_func(void *arg)
{
	int id = *(int *)arg;
	unsigned long t = random()%NUM_INTS;
	//unsigned long t = id * 2 * 128 *1024;
	printf("In threadID : %d ... map[%lu] : '%d'\n",id,t, map[t]);
	//sleep(1);
	return arg;
}

int main()
{
	pthread_t random_faults[NUM_THREADS];
	pthread_t seq_fault;
	int fd, i;
	//int j;
	int ids[NUM_THREADS];
	//unsigned long tempL;
	fd = open(DEVICE_NAME, O_RDWR);

	if(fd<0)
	{
		printf("Error on opening fd.\n");
		exit(1);
	}
	map = mmap(NULL,FILE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	printf("done mmaping\n");
	pthread_create(&seq_fault, NULL, seq_fault_func, NULL);

	for(i = 0; i < NUM_THREADS; i++)
	{
		ids[i] = i+1;
		pthread_create(&random_faults[i], NULL, random_fault_func, (void *)&ids[i]);
	}
	for(i = 0; i < NUM_THREADS; i++)
		pthread_detach(random_faults[i]);
	pthread_join(seq_fault, NULL);
	munmap(map,FILE_SIZE);
	return 0;
}

/*
 * linux_defs.h
 *
 *  Created on: Jun 1, 2012
 *      Author: jaspal
 */

#ifndef LINUX_DEFS_H_
#define LINUX_DEFS_H_

/*
 * borrowed definitions from the linux kernel
 */
#define BIT(nr)				(1UL << (nr))
#define BIT_MASK(nr)		(1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)
#define DIV_ROUND_UP(n,d)	(((n)+(d)-1) / (d))
#define BITS_PER_BYTE		8
#define BITS_TO_LONG(nr)	DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(long))
#define BITS_PER_LONG		(sizeof(unsigned long) * BITS_PER_BYTE)

/*
 * create_bitmap - to create the bitmap
 */
unsigned long *create_bitmap(unsigned long bits)
{
	unsigned long *bitmap_array;
	bitmap_array = (unsigned long *)malloc(BITS_TO_LONG(bits) * sizeof(unsigned long));
	memset(bitmap_array, 0, sizeof(unsigned long)* BITS_TO_LONG(bits));
	return bitmap_array;
}


/*
 * set_bit - to set the bit of the given offset
 * it is same as that of linux kernel
 */
static void set_bit(unsigned long bit, unsigned long *bitmap_array)
{
	unsigned long mask = BIT_MASK(bit);
	unsigned long *p = ((unsigned long *)bitmap_array) + BIT_WORD(bit);
	*p |= mask;
	return;
}

/*
 * test_bit - to check if the bit is set or not
 */
static int test_bit(unsigned long bit, unsigned long *bitmap_array)
{
	unsigned long mask = BIT_MASK(bit);
	unsigned long *p = ((unsigned long *)bitmap_array) + BIT_WORD(bit);
	return ((*p & mask) != 0);
}


/*
 * clean_bitmap - to free the memory of the bitmap
 */

static void clean_bitmap(unsigned long *bitmap_array)
{
	free(bitmap_array);
	bitmap_array=NULL;
	return;
}




#endif /* LINUX_DEFS_H_ */

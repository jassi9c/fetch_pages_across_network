#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>


#include <sys/stat.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "fault_handler.h"


#define PAGESIZE 	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))

#define PORT 3368
#define hostname "127.0.0.1"

#define RAMFILE	"fault_handler"
static int NUM_PAGES = 1000 ; // 10K pages is 39 MB , 25600 is 100MB on 32 bit systems
#define PAGESIZE	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))
#define NUM_INTS	(NUM_INTS_PER_PAGE*NUM_PAGES)
#define FILE_SIZE	(NUM_INTS*sizeof(int))

static int fd;			// fd for server
static int sockfd ;
static int *map;		// the pointer to ram


int kfd;				// fd for kvm
int rfd;				// fd for ram-file

struct dirty_log_info {
	int index ;
	unsigned long size;
	unsigned long *dirty_map;
};

char *qemu_client_flag = NULL;
struct dirty_log_info new_dirty_pages ;

int setup_conn()
{
	int n;
	struct sockaddr_in server_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		printf("socket connection error");
		return -1;
	}
	server = gethostbyname(hostname);
	if(server == NULL)
	{
		printf("No such host exists.. EXITING");
		exit(0);
	}
	memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	memmove((char *)&server_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
	server_addr.sin_port = htons(PORT);
	if(connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		perror("connect");
		return -1;
	}

	return 0;
}

int make_conn_to_kvm()
{
	char name[]="modified kvm on the rocks1";
	int index ;
	kfd = open("/dev/kvm",O_RDWR);
	if(kfd==-1)
	{
		return -1;
	}
	index = ioctl(kfd , KVM_GET_VM_INDEX , &name);

	if(index==-1)	// if dst is on different physical machine , assuming all times that max 2 vms are operational
	{
		char nname[]="modified kvm on the rocks";
		index = ioctl(kfd , KVM_GET_VM_INDEX , &nname);
		if(index==-1)
		{
			printf("could not get index. check names.\n");
			exit(-1);
		}
	}

	printf("index = %d\n",index);

	new_dirty_pages.index = index;

	new_dirty_pages.size = 1; // set this to 1 to set is_migration_active in kvm , this will be updated after ioctl is complete

	int r = ioctl(kfd , KVM_GET_DIRTY_LOG_SIZE_BY_VM_INDEX , &new_dirty_pages);

	new_dirty_pages.dirty_map = calloc(new_dirty_pages.size , sizeof(unsigned long));

	return 0;
}

int open_ram_file()
{
	rfd = open(RAMFILE , O_RDONLY);
	if(rfd<0)
	{
		printf("Could not open %s. Terminating program.\n",RAMFILE);
		exit(-1);
	}
	/*
	struct stat64 st;
	int result = stat64(RAMFILE,&st);
	unsigned long fs = st.st_size;
	*/
	NUM_PAGES = 65536 ;
	//printf("File size in MB is %lu and NUM_PAGES = %d \n",fs/1024/1024,NUM_PAGES);
	map = mmap(0,FILE_SIZE , PROT_READ , MAP_SHARED , rfd , 0 );
	if(map== MAP_FAILED)
	{
		printf("Could not mmap the file.Terminating.\n");
		close(rfd);
		return -1;
	}
	printf("mmap successful.\n");
	return 0;
}

int wait_for_migration_to_start()
{
	int qemu_client_fd = open("/dev/shm/qemu_client_flag", O_RDONLY, 0600);
	if(fd < 0)
	{
		printf("error in opening qemu_client_fd\n");
		return -1;
	}
	qemu_client_flag = mmap(NULL, sizeof(char), PROT_READ, MAP_SHARED, qemu_client_fd, 0);
	close(qemu_client_fd);

	pthread_cond_t dummy_cond = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t dummy_mutex = PTHREAD_MUTEX_INITIALIZER ;
	struct timespec timeToWait;
	timeToWait.tv_sec = 0;
	timeToWait.tv_nsec = 1000000;	// 1 milli-second

	pthread_mutex_lock(&dummy_mutex);

	while(qemu_client_flag[0]!='2')
		pthread_cond_timedwait(&dummy_cond,&dummy_mutex,(const struct timespec * __restrict__)&timeToWait);

	pthread_mutex_unlock(&dummy_mutex);

	return 0;
}

void send_pages()
{
	printf("%s executing...\n",__func__);
	int r;
	unsigned long i,t;
	int pages_sent = 0;
	while(qemu_client_flag && qemu_client_flag[0]=='2'){
		pages_sent = 0;
		printf("doing the KVM_GET_NEW_DIRTY_LOG_BY_VM_INDEX ioctl\n");
		r= ioctl(kfd , KVM_GET_NEW_DIRTY_LOG_BY_VM_INDEX , &new_dirty_pages);
		printf("ioctl complete\n");
		for(i=0;i<new_dirty_pages.size;i++){
			if(new_dirty_pages.dirty_map[i]){
				r = send(sockfd, &i, sizeof(unsigned long), 0);	// send the pgoff
				r = send(sockfd, map +i*NUM_INTS_PER_PAGE , PAGESIZE , 0 );							// send the data
				pages_sent++;
			}
		}
		if(pages_sent)
			printf("Sent %d pages\n",pages_sent);
	}
}

int main()
{
	if(setup_conn() < 0)
	{
		printf("Error in connection ");
		close(fd);
		return -1;
	}

	if(make_conn_to_kvm())
		return -1;

	if(open_ram_file())
		return -1;

	if(wait_for_migration_to_start())
		return -1;

	send_pages();

	return 0 ;
}

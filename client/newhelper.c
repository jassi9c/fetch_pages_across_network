#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "fault_handler.h"
#include "linux_defs.h"

#include "minilzo.h"

#define PAGESIZE 	getpagesize()
#define NUM_INTS_PER_PAGE (PAGESIZE/sizeof(int))

#define PORT 3367
#define hostname "127.0.0.1"

static unsigned long NUM_PAGES ;

static int fd;
static int sockfd ;
static int *map = NULL;
static unsigned long size_ul = sizeof ( unsigned long );

unsigned long *pages_bitmap = NULL;

pthread_mutex_t bgod_mutex  = PTHREAD_MUTEX_INITIALIZER ;
pthread_mutex_t odioctl_mutex  = PTHREAD_MUTEX_INITIALIZER ;
pthread_cond_t  bgod_cond   = PTHREAD_COND_INITIALIZER;
pthread_cond_t  odioctl_cond   = PTHREAD_COND_INITIALIZER;
pthread_cond_t  odioctl_cond2   = PTHREAD_COND_INITIALIZER;

pthread_t bg_thread , od_thread;

static int od_ready = 0;
static int bg_can_start = 1;


int pages_fetched_by_bg = 0;
int pages_fetched_by_od = 0;

unsigned long pgoff;

int total_data =0;
char *qemu_client_flag = NULL;

// the following two #defines give errors for variables in,out
//#define IN_LEN 		(getpagesize()*128)
//#define OUT_LEN 	(IN_LEN + IN_LEN / 16 + 64 + 3)
#define IN_LEN 		(4096*128)
#define OUT_LEN		((4096*128)+(4096*128)/16 + 64 +3)


unsigned char __LZO_MMODEL in  [ IN_LEN ];
unsigned char __LZO_MMODEL out [ OUT_LEN ];


/* Work-memory needed for compression. Allocate memory in units
 *  * of 'lzo_align_t' (instead of 'char') to make sure it is properly aligned.
 *   */

#define HEAP_ALLOC(var,size) \
    lzo_align_t __LZO_MMODEL var [ ((size) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) ]

static HEAP_ALLOC(wrkmem, LZO1X_1_MEM_COMPRESS);


int setup_conn()
{
	int n;
	struct sockaddr_in server_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		printf("socket connection error");
		return -1;
	}
	server = gethostbyname(hostname);
	if(server == NULL)
	{
		printf("No such host exists.. EXITING");
		exit(0);
	}
	memset(&server_addr,0,sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	memmove((char *)&server_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
	server_addr.sin_port = htons(PORT);
	if(connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		perror("connect");
		return -1;
	}

	n = recv(sockfd , &NUM_PAGES , sizeof(unsigned long) , MSG_WAITALL);
	printf("Got %lu as NUM_PAGES from server\n",NUM_PAGES);

	return 0;
}

int reserve_memory_address_space()
{
	int zfd;
	int ret ;
	struct fh_reserve_memory_request mem_req ;
	zfd = open("/dev/zero",O_RDWR | O_TRUNC);
	if(zfd < 0)
	{
		perror("open");
		return -ENOMEM;
	}
	printf("Reserving %lu bytes and npages = %lu\n",NUM_PAGES*PAGESIZE,NUM_PAGES);
	map = mmap(NULL, NUM_PAGES*PAGESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, zfd, 0);

	if(map == MAP_FAILED)
	{
		close(zfd);
		perror("mmap");
		return -ENOMEM;
	}
	close(zfd);

	mem_req.addr = (unsigned long)map;
	mem_req.npages = NUM_PAGES;
	ret = ioctl(fd, FAULT_HANDLER_RESERVE_MEMORY, &mem_req);
	if(ret)
	{
		printf("error in ioctl : FAULT_HANDLER_RESERVE_MEMORY\n");
		perror("ioctl");
		close(fd);
		munmap(map, NUM_PAGES*PAGESIZE);
		return 1;
	}
	int qemu_client_fd = open("/dev/shm/qemu_client_flag", O_RDONLY, 0600);
	if(fd < 0)
	{
		perror("open");
		return -ENOMEM;
	}
	qemu_client_flag = mmap(NULL, sizeof(char), PROT_READ, MAP_SHARED, qemu_client_fd, 0);
	close(qemu_client_fd);

	if(qemu_client_flag == MAP_FAILED)
	{
		printf("qemu_client_flag == MAP_FAILED\n");
		munmap(qemu_client_flag, sizeof(char));
		return -EINVAL;		
	}
	return 0;
}

void* bg_function(void* arg)
{
	unsigned long last_pgoff = 0;
	int ret;
	int max_processed;
	int r;
	unsigned long len_of_compressed_data;
	unsigned long in_len;
	printf("bg thread starting....\n");

	while(1)
	{
		pthread_mutex_lock(&bgod_mutex);
		while(od_ready==1)
			pthread_cond_wait(&bgod_cond,&bgod_mutex);
		if(last_pgoff>=NUM_PAGES){
				pthread_mutex_unlock(&bgod_mutex);
				break;
		}
		if(test_bit(last_pgoff, pages_bitmap)){
			//last_pgoff+=MAX_PAGES_IN_ONE_TRIP;
			//total_fetched+=MAX_PAGES_IN_ONE_TRIP;
			//pthread_mutex_unlock(&bgod_mutex);
			//continue;
			max_processed=0;
			goto outt;
		}
		ret = send(sockfd, &last_pgoff, size_ul, 0);
		if(ret <= 0)
		{
			printf("socket write error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		max_processed = (last_pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - last_pgoff ;

		in_len=max_processed*PAGESIZE;
		// recv length of compressed data
		recv(sockfd,&len_of_compressed_data,sizeof(unsigned long),MSG_WAITALL);
		// recv compressed data
		ret = recv(sockfd, out, len_of_compressed_data, MSG_WAITALL);
		// decompress it
		r = lzo1x_decompress(out,len_of_compressed_data,in,&in_len,NULL);
		// paste it in memory
		memcpy(map + pgoff * NUM_INTS_PER_PAGE,in,in_len);


		/*
		ret = recv(sockfd, map + last_pgoff * NUM_INTS_PER_PAGE, PAGESIZE*max_processed, MSG_WAITALL);
		if(ret < 0)
		{
			printf("socket read error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		*/
		ioctl(fd, FAULT_HANDLER_FAULT_PROCESSED,&last_pgoff);
		unsigned long nret;
		for(nret=last_pgoff;nret<last_pgoff+max_processed;nret++){
			set_bit(nret, pages_bitmap);
		}
		pages_fetched_by_bg+=max_processed;
		outt:
		//total_fetched+=max_processed;
		pthread_mutex_unlock(&bgod_mutex);
		last_pgoff+=128;
		if(last_pgoff>=NUM_PAGES || pages_fetched_by_bg+pages_fetched_by_od>=NUM_PAGES)
			break;
	}
	printf("bg: pages fetched  : %d\n",pages_fetched_by_bg);
	return NULL;
}

void* od_function(void* arg)
{
	int ret;
	int max_processed ;
	unsigned long len_of_compressed_data;
		int r;
		unsigned long in_len;
	while(1)
	{
		if(pages_fetched_by_bg+pages_fetched_by_od>= NUM_PAGES*2)
		break;

		pthread_mutex_lock(&odioctl_mutex);
		while(od_ready==0)
			pthread_cond_wait(&odioctl_cond,&odioctl_mutex);
		pthread_mutex_lock(&bgod_mutex);

		if(test_bit(pgoff, pages_bitmap)){
			//pages_fetched_by_od+=MAX_PAGES_IN_ONE_TRIP;
			goto done;
		}
		ret = send(sockfd, &pgoff, size_ul, 0);
		if(ret <= 0)
		{
			printf("socket write error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		max_processed = (pgoff <= NUM_PAGES - MAX_PAGES_IN_ONE_TRIP ) ?  MAX_PAGES_IN_ONE_TRIP : NUM_PAGES - pgoff ;


		in_len=max_processed*PAGESIZE;
		// recv length of compressed data
		recv(sockfd,&len_of_compressed_data,sizeof(unsigned long),MSG_WAITALL);
		// recv compressed data
		ret = recv(sockfd, out, len_of_compressed_data, MSG_WAITALL);
		// decompress it
		r = lzo1x_decompress(out,len_of_compressed_data,in,&in_len,NULL);
		// paste it in memory
		memcpy(map + pgoff * NUM_INTS_PER_PAGE,in,in_len);


		/*
		ret = recv(sockfd, map + pgoff * NUM_INTS_PER_PAGE, PAGESIZE*max_processed, MSG_WAITALL);
		total_data +=ret;
		if(ret < 0)
		{
			printf("socket read error");
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			exit(-1);
		}
		*/
		pages_fetched_by_od+=max_processed;
		//total_fetched+=max_processed;
		unsigned long nret;
		for(nret=pgoff;nret<pgoff+max_processed;nret++){
			set_bit(nret, pages_bitmap);
		}
		done:
		od_ready=0;
		pthread_cond_signal(&bgod_cond);
		pthread_mutex_unlock(&bgod_mutex);
		pthread_cond_signal(&odioctl_cond2);
		pthread_mutex_unlock(&odioctl_mutex);
		if(pages_fetched_by_bg+pages_fetched_by_od>= NUM_PAGES*2)
			break;

	}
	printf("od: pages fetched :  %d\n",pages_fetched_by_od);
	return NULL;
}

int helper_fn()
{
	printf("%s executing\n",__func__);

	int ret;

	while(1)
	{
		if(pages_fetched_by_bg+pages_fetched_by_od>=NUM_PAGES*2)
			break;
		ret = ioctl(fd, FAULT_HANDLER_GET_FAULT, &pgoff);
		if(ret<0)
		{
			close(fd);
			munmap(map, NUM_PAGES*PAGESIZE);
			printf("ioctl error , check FAULT_HANDLER_GET_FAULT and fd\n");
			return 1;
		}
		if(pgoff==-1)	// the driver is telling that all pages have been fetched. so stop.
			break;
		pthread_mutex_lock(&odioctl_mutex);
		od_ready=1;
		pthread_cond_signal(&odioctl_cond);
		pthread_cond_wait(&odioctl_cond2,&odioctl_mutex);
		ioctl(fd, FAULT_HANDLER_FAULT_PROCESSED,&pgoff);
		//if(bg_can_start && pages_fetched_by_od>20000){
			//pthread_create(&bg_thread , NULL , &bg_function , NULL);
			//bg_can_start=0;
		//}
		if(pages_fetched_by_bg+pages_fetched_by_od>=NUM_PAGES*2)
		{
			pthread_mutex_unlock(&odioctl_mutex);
			break;
		}
		else
			printf("%s pages_transferred = %d\n",__func__,pages_fetched_by_od);
		pthread_mutex_unlock(&odioctl_mutex);
	}
	//pthread_join(bg_thread,NULL);
	return 0;
}

int main()
{

	if (lzo_init() != LZO_E_OK)
	{
		printf("internal error - lzo_init() failed !!!\n");
		printf("(this usually indicates a compiler bug - try recompiling\nwithout optimizations, and enable '-DLZO_DEBUG' for diagnostics)\n");
		return 3;
	}

	int ret;
	unsigned long pgoff;

	if(setup_conn() < 0)
	{
		printf("Error in connection ");
		close(fd);
		return 1;
	}
	fd = open(DEVICE_NAME,O_RDWR);
	reserve_memory_address_space();
	pages_bitmap = create_bitmap(NUM_PAGES);
	printf("pages_bitmap initialized. pages_bitmap[0]=%d\n",pages_bitmap[0]);
	pthread_create(&od_thread,NULL,&od_function,NULL);

	if(helper_fn())
	{
		printf("doing munmap sksk\n");
		munmap(map, NUM_PAGES*PAGESIZE);
		close(fd);
		return -1;
	}
	pthread_join(od_thread,NULL);
	printf("done\n");
	//munmap((void*)map, NUM_PAGES*PAGESIZE);
	printf("munmap skipped\n");
	close(fd);
	close(sockfd);
	clean_bitmap(pages_bitmap);

	return 0;
}
